import Vue from 'vue'
import Router from 'vue-router'

import Home from '~/pages/home/Home'
import PageWrapper from '~/pages/home/PageWrapper'
import NotFound from '~/pages/NotFound'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: PageWrapper,
        children: [
          {
            path: '/hotel',
            components: Home,
            name: 'Home'
          }
        ]
      }
      ,
      {
        path: '/*',
        component: NotFound,
        name: 'NotFound'
      }

    ]
  })
}
