// import moment from 'moment';

export default { 
    setCookiesSearch(obj){ 
        var objWarp = {}
        var newObj = {}  
        var arr = new Array() 
        // new add
        newObj = {...obj}  
        delete newObj.historyPosition  
        arr.push(newObj)

        //get last added cookies
        var dataCookie = this.getCookieHistory();
        var counterLimitCookie = 1;
        var limitCookieData = 5; 
         
        if( obj.historyPosition !== undefined ){ 
            dataCookie.splice( obj.historyPosition ,1)
        }   
        
        if (null != dataCookie && dataCookie.length > 0) {  
            for (var i = 0; i < dataCookie.length; i++) {
                if (obj.selectedLocation.maintext != dataCookie[i].selectedLocation.maintext && 
                    counterLimitCookie < limitCookieData) {
                    var o = {}
                    o.selectedLocation = dataCookie[i].selectedLocation
                    o.dateInfo = dataCookie[i].dateInfo;
                    o.guestRoomInfo = dataCookie[i].guestRoomInfo
                    o.filter = dataCookie[i].filter 
                    arr.push(o);
                    counterLimitCookie++;
                }
                if(counterLimitCookie==limitCookieData) break;
            }
        }  
        objWarp.SearchHotelhistory = arr;

        var val =  JSON.stringify(objWarp.SearchHotelhistory).replace(/,/g,'|') ;            
        let d = new Date()   
        d.setTime(d.getTime() + (1*24*60*60*1000))
        let expires = "expires="+ d.toGMTString()  
        const cok = "SearchHotelhistory"+"=" + val + ";" + expires + ";domain"+"="+""+ window.location.hostname +""+";path=/;";
           
        document.cookie =  cok
        // window.location.reload()
    }
    , getCookieHistory (){  
        
        var arrCookie = document.cookie.replace(/\|/g,',').split(';');
        var x,res; 
        for (var i = 0; i < arrCookie.length; i++) {
            x = arrCookie[i].substr(0,arrCookie[i].indexOf("=")); 
            x = x.replace(/^\s+|\s+$/g,"");	
            if (x == 'SearchHotelhistory') {
                res = arrCookie[i].substr(arrCookie[i].indexOf("=") + 1);
            }
        } 
        if ( res ) { 
            const objWarp = JSON.parse(res);
            return objWarp ;
        } else { 
            return null;
        }
    }
    , clearCookieInPage () {
        var date = new Date();
        date.setTime(date.getTime()+((-30)*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
        document.cookie = "SearchHotelhistory"+"="+""+expires+";domain"+"="+""+window.location.hostname+""+";path=/;";
        
        let element = document.querySelector('#swiperHistory') 
        element.parentNode.removeChild(element)  
        // let element2 = document.querySelector('#latestSearch') 
        // element2.parentNode.removeChild(element2)  
        
    } 
  }
  