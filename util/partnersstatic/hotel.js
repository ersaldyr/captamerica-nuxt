export default [
    [
        {
            id:0,
            url: "fave.svg",
            label:"Fave Hotel"
        },
        {
            id:1,
            url: "aston.svg",
            label:"Aston Hotel"
        },
        {
            id:2,
            url: "amaris.svg",
            label:"Amaris Hotel"
        },
        {
            id:3,
            url: "santika.svg",
            label:"Santika Hotel"
        },
        {
            id:4,
            url: "pop-hotel.svg",
            label:"Pop Hotel"
        },
        {
            id:5,
            url: "harris.svg",
            label:"Harris Hotel"
        },
        {
            id:6,
            url: "redplanet.svg",
            label:"Red Planet Hotel"
        },
        {
            id:7,
            url: "whiz.svg",
            label:"Whiz Hotel Group"
        },
        {
            id:8,
            url: "zest.svg",
            label:"zest Hotel Group"
        },
        {
            id:9,
            url: "swiss-belhotel.svg",
            label:"Swiss-bell Hotel"
        }                
    ],
    [
        {
            id:10,
            url: "maxone.svg",
            label:"Maxone Hotel"
        },
        {
            id:11,
            url: "the-101.svg",
            label:"The 101"
        },
        {
            id:12,
            url: "citradream.svg",
            label:"Citra Dream"
        },
        {
            id:13,
            url: "le-green.svg",
            label:"Le Green Hotel"
        },
        {
            id:14,
            url: "cordella.svg",
            label:"Cordela Hotel"
        }                 
    ]
];