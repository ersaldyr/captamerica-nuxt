import moment from 'moment'

export const state = () => ({
  hotelSearchData:{
    selectedLocation: {},
    dateInfo:{
      checkIn: moment().format(),
      checkOut: moment().add('1','d').format() 
    },
    guestRoomInfo:{ 
      guestCount: 2,
      roomCount: 1,
    },
    filter:{
      price: [0, 5000001],
      star: [false, false, false, false, false]
    }
  }
})

export const mutations = {
  updateHotelSearchData (state,payload){
    state.hotelSearchData = {...state.hotelSearchData, ...payload}
  },
  setSelectedLocation (state, payload){
    state.hotelSearchData.selectedLocation = payload;
  }, 
}