export default {
  methods: {
    debounce(func, delay) {
      let timeoutID = null
      return function fun1() {
        clearTimeout(timeoutID)
        const args = arguments
        const self = this
        timeoutID = setTimeout(function fun2() {
          func.apply(self, args)
        }, delay)
      }
    }
  }
}
