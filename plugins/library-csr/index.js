/**
 * import and init global filters
 */

import Vue from 'vue'

import VueSlider from 'vue-slider-component'
import VueTouch from 'vue-touch'
//import BodyScrollLock from 'body-scroll-lock'
import VueAwesomeSwiper from 'vue-awesome-swiper'

Vue.use(VueTouch)
//Vue.use(BodyScrollLock)
Vue.use(VueAwesomeSwiper)
Vue.component(VueSlider)
