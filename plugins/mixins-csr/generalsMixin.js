export default {
    data(){
        return {
            scrollTopValue: 0,
        }
    }, 
    methods: {
        isScrollDisabled(status){ 
            let body = document.querySelector('body');
            if (status) { 
                this.scrollTopValue = window.scrollY * -1;
                body.classList.add('scroll-lock'); 
                body.style.top = this.scrollTopValue + 'px'; 
            }
            else {
                body.classList.remove('scroll-lock');  
                window.scrollTo(0, this.scrollTopValue * -1);  
             } 
        },
        getOS: function() {
          var userAgent = window.navigator.userAgent,
              platform = window.navigator.platform,
              macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
              windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
              iosPlatforms = ['iPhone', 'iPad', 'iPod'],
              os = null;

          if (macosPlatforms.indexOf(platform) !== -1) {
            os = 'mac os';
          } else if (iosPlatforms.indexOf(platform) !== -1) {
            os = 'ios';
          } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os = 'windows';
          } else if (/Android/.test(userAgent)) {
            os = 'android';
          } else if (!os && /Linux/.test(platform)) {
            os = 'linux';
          }

          return os;
        },
        hideIosSoftKeyboard: function(refname){
            if (this.getOS()=='ios') {
                let el = this.$refs[refname]
                if (el) {
                    el.blur()
                }
            }
        } 
    }
}