import moment from 'moment'; 
import {monthsName,daysName} from '@/util/lang/id'

export default {
    computed:{  
    },
    methods:{
        dateTextFormat( formatedDate ){
            const selectedDay = moment(formatedDate) 
            const day = daysName[selectedDay.day()]
            const date = selectedDay.date(); 
            const month = monthsName[selectedDay.month()]
            const year = selectedDay.year()  
            return  day.substr(0,3) + ', ' + date + ' ' + month.substr(0,3) + ' ' + ( year + '').substr(2,3) 
        },
    }
}