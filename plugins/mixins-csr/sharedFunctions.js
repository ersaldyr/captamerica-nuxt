export default {
    methods: {
        forceAbsoluteURL: function(url){
            if (/^http[s]*:\/\/[\w]+/i.test(url)) {
                 return  url
            }  
            return '//' + url
        },
        getMobileOperatingSystem:function(){
            let userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (/android/i.test(userAgent)) {
                return "https://app.appsflyer.com/com.pegipegi.android?pid=internal&c=sp_main_homepage"
            }
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return "https://app.appsflyer.com/id1054846518?pid=internal&c=sp_main_homepage"
            }
            return "#"
        },
        getCookieSso: function(a) {
            // for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) {
            //     for (var e = c[d];
            //         " " == e.charAt(0);) e = e.substring(1)
            //     if (0 == e.indexOf(b)) return e.substring(b.length, e.length)
            // }
            return ""
        }
    }
  }
  