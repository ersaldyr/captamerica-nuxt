import Vue from 'vue'

import generalMixin from './generalsMixin'
import sharedFunctions from './sharedFunctions'
import textFormatMixin from './textFormatMixin'

Vue.mixin(generalMixin)
Vue.mixin(sharedFunctions)
Vue.mixin(textFormatMixin)