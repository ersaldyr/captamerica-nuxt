/**
 * import and init global filters
 */

import Vue from 'vue'

import toTitleCase from './toTitleCase'
import toCurrency from './toCurrency'

Vue.filter('toTitleCase', toTitleCase)
Vue.filter('toCurrency', toCurrency)
