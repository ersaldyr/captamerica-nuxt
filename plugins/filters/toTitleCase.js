export default (string) => {
  if (string === undefined || string === '') {
    return string
  } else {
    return string.toLowerCase().replace(/\b(\w)/g, (s) => s.toUpperCase())
  }
}
