import { initInstance } from './base.services'
import axios from 'axios'
import { endpoints } from '@/config'

const bannerInstance = axios.create({
  baseURL: endpoints.bannerUrl
})

export { bannerInstance }