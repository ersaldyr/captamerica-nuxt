import axios from 'axios'

const CancelToken = axios.CancelToken
export const source = CancelToken.source()

export function initInstance(params) {
  return axios.create(params)
}

export const throwRequest = axios.isCancel

export function configContentHeader(params) {
  switch (params) {
    case 'json':
      return 'application/json'
    case 'form-data':
      return 'multipart/form-data'
    case 'url':
      return 'application/x-www-form-urlencoded'
    case 'xml':
      return 'application/xml'
    default:
      return 'application/json'
  }
}
