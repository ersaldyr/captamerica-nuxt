const BASE_URL = process.env.baseUrl;

export default {
  serviceName: {
    baseUrl: BASE_URL,
    bannerUrl: `${BASE_URL}/banner-api`
  }
}
