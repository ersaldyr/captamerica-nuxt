
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }

    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/Loading',
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss',
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/swiper/dist/css/swiper.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/mixins',
    '@/plugins/filters',
    '@/plugins/directives',
    {src:'@/plugins/mixins-csr', mode: 'client'},
    {src:'@/plugins/library-csr', mode: 'client'}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    //'@nuxtjs/eslint-module'
    '@nuxtjs/router'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/style-resources'
  ],

  styleResources: {
    scss: [
      '@/assets/scss/abstracts/_variables.scss',
      '@/assets/scss/abstracts/_functions.scss',
      '@/assets/scss/abstracts/_mixins.scss',
      '@/assets/css/general.scss',  
      '@/assets/css/helper.scss',  
      '@/assets/css/color.scss',  
      '@/assets/css/pagewrapper.scss'
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */

    // extractCss: true,
    extend(config, ctx) {
    }
  },

  env: {
    // baseUrl: process.env.BASE_URL
    baseUrl: 'https://apix.pegipegi.com'
  },

  server: {
    host: '0.0.0.0', // default: localhost
  }
}
